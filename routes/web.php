<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return view('master.master');
} );

Route::get('/', 'DashboardController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index');
Route::get('/settings', 'SettingsController@index');
Route::get('emails/sent', 'EmailController@sent')->name('email.sent');
Route::get('emails/trash', 'EmailController@trash')->name('email.trash');
Route::get('email/starred/{id}', 'EmailController@starred')->name('email.starred');
Route::get('emails/starred-emails', 'EmailController@starred_emails');
Route::get('email/unstarred/{id}', 'EmailController@unstarred')->name('email.unstarred');
Route::get('email/download_attachment/{attachment}', 'EmailController@download_attachment');
Route::get('email/read/{id}', 'EmailController@read')->name('email.read');
Route::get('email/restore/{id}', 'EmailController@restore')->name('email.restore');

Route::post('test/', 'StudentAjaxController@test');
Route::get('get-emails', 'EmailController@ajaxGetAllReceipients');
Route::post('get-receipients/', 'EmailController@ajaxGetReceipients');




Route::resource('emails', 'EmailController');

Route::post('chats', 'ChatController@store');
Route::get('get-chats', 'ChatController@getChats');
Route::get('get-all-chat-to', 'ChatController@getAllChatTo');
Route::post('get-chat-recipient', 'ChatController@getChatRecipient');

Route::get('download/{file}', 'DownloadController@downloadFile')->name('download.file');
Route::get('download-email/{file}', 'DownloadController@downloadEmail')->name('download.email');


Route::resource('events','EventController');
Route::resource('programs','ProgramController');


Route::get('courses/register-courses', 'CourseController@register');
Route::resource('courses', 'CourseController');


Route::resource('students',   'StudentController');
Route::resource('materials',  'MaterialController');

Route::get('assignment/{assignment}', 'SubmitAssignmentController@submitShow')->name('submit.assignment');
Route::resource('assignments',  'AssignmentController');

Route::get('fetchPercentage/{id}', 'ExamController@fetchPercentage');


Route::resource('cs',   'CourseStudentController');
Route::resource('lecturers',   'LecturerController');
Route::resource('exams',   'ExamController');
Route::resource('submissions',   'SubmitAssignmentController');
Route::resource('makes',   'MakeController');
Route::resource('grades',   'GradeController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
