$("#upload_button").click(function(e) {
    $("#profile_image").click();
    e.preventDefault();
    $(this).mouseleave(function() {

    });

    $('#profile_image').bind('change', function() {
        $(".filePath").html("");
        $(".filePath").html($("#profile_image").val());

        let fileSize = this.files[0].size;

        //check if the size is bigger than required
        if (fileSize > 30000) {
            alert("file size is bigger than the required size, select another file");
            $('#profile_image').val("");
            $(".filePath").html("");
        } else {
            $('.action-button').removeAttr('disabled'); //enable button
        }


    });

})