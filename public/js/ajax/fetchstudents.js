$(document).ready(function() {
    $("#ajaxStart").hide();
    $("#course").change(function() {

        let value = $("#course").val();


        $.ajax({
            type: 'post',
            url: 'test',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': value,
            },
            success: function(data) {
                console.log(data.response);
                $(".student-hide").remove();
                $("#studentSelect").append(data.response);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("errorThrown: " + errorThrown);
            }

        });


    });



    $("#type").change(function() {

        let value = $("#type").val();

        let URL_PATH = $("#URL_PATH").val() + '/fetchPercentage/' + value;
        console.log(URL_PATH);
        $.ajax({
            type: 'get',
            url: URL_PATH,
            success: function(data) {
                $("#percentage").val(data);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("errorThrown: " + errorThrown);
            }

        });


    });
});