function reset_resources() {
    //Hide The notification icons
    $('#sendingIcon').hide();
    $("#notify").hide();
    $("#inputError").hide();

    //Reset the local storage so that anytime that the page is reloaded we will 
    //get a fresh message.
    localStorage.setItem("lastID", null);
    localStorage.setItem("lastRecievedID", null);
}

function open_chat_box() {
    $(".chat-dropdown").addClass("open");
}



$('.chatSound').on('change', function() {
    let option = this.value;
    localStorage.setItem("playSound", option);
});


$('.chatBox').on('change', function() {
    let option = this.value;
    localStorage.setItem("chatBox", option);
});

$('.newChat').on('change', function() {
    let option = this.value;
    localStorage.setItem("newChatBox", option);
});

$(document).ready(function() {

    //Reset the nesessary resources
    reset_resources();


    if (localStorage.getItem("chatBox") == "on") {
        open_chat_box();
    }

    //Check if the users browser can support local storage 
    //because the application depends heavily on it.
    if (typeof(Storage) !== "undefined") {

        $('#message').keypress(function(event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                send();
            }
            event.stopPropagation();
        });


        //If the sendMessage button is clicked
        $('#sendMessage').click(function(event) {
            send();
        });


        //Get all the messages ninitially when the page loads
        get_messages('manual');

        //Get all respondents
        getAll();

        //Get new message after every 5 seconds
        setInterval(function() { get_messages('auto'); }, 5000);

    } else {
        // Sorry! No Web Storage support..
    }


});

function send() {
    //Retireve the form data
    let chat_to = $('#chat_to').val();
    let message = $('#message').val();



    //check if they data were supplied.
    if (chat_to.length >= 1 && message.length >= 1) {

        //show the sending icon
        $('#sendingIcon').show();

        let URL_PATH = $("#URL_PATH").val() + '/chats';
        //configure the ajax request
        $.ajax({
            type: 'post',
            url: URL_PATH,
            data: {
                '_token': $('input[name=_token]').val(),
                'chat_to': chat_to,
                'message': message
            },
            success: function(data) {
                //Message was successful
                setTimeout(function() {
                        //get the latest message
                        get_messages();
                        //Clear the message field
                        $('#message').val('');
                        //hide the sending Icon
                        $('#sendingIcon').hide();
                    },
                    1200);


            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("errorThrown: " + errorThrown);
            }

        });
    } else {

        $("#inputError").show();
        setTimeout(function() { $("#inputError").fadeOut(); }, 3000);
    }
}

function get_messages(mode) {

    let URL_PATH = $("#URL_PATH").val() + '/get-chats';
    console.log(URL_PATH);
    $.ajax({
        type: 'get',
        url: URL_PATH,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            console.log('data is = ' + data);
            let localId = parseInt(localStorage.getItem("lastID"));
            let databaseId = parseInt(data.lastID);


            let localLastRecievedID = parseInt(localStorage.getItem("lastRecievedID"));
            let databaseLastRecievedID = parseInt(data.lastRecievedID);


            if (databaseLastRecievedID === localLastRecievedID) {
                console.log("databaseLastRecievedID : " + databaseLastRecievedID);
                console.log("localLastRecievedID : " + localLastRecievedID);
            } else {
                console.log("databaseLastRecievedID : " + databaseLastRecievedID);
                console.log("localLastRecievedID : " + localLastRecievedID);
                localStorage.setItem("lastRecievedID", databaseLastRecievedID);

                if (mode === 'auto') {
                    $("#notify").fadeIn();
                    setTimeout(function() { $("#notify").fadeOut(); }, 5000);
                }

                //Notify user of a new message
                var x = document.getElementById("myAudio");
                if (localStorage.getItem("playSound") == "on") {
                    x.play();
                }

                //open the chat box
                if (localStorage.getItem("newChatBox") == "on" && mode === 'auto') {
                    open_chat_box();
                }
            }

            if (databaseId === localId) {
                //console.log("its equal")
            } else {

                localStorage.setItem("lastID", parseInt(databaseId));
                $('#remove').remove();

                $('#messages').append(data.response);
            }

        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("errorThrown: " + errorThrown);
        }

    });

}

function getAll() {
    let URL_PATH = $("#URL_PATH").val() + '/get-all-chat-to';
    $.ajax({
        type: 'get',
        url: URL_PATH,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            $('#receivers').append(data.response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("errorThrown: " + errorThrown);
        }

    });

}


$('.receipient').change(function() {
    let option = this.value;
    let URL_PATH = $("#URL_PATH").val() + '/get-chat-recipient';
    console.log(option);
    $.ajax({
        type: 'post',
        url: URL_PATH,
        data: {
            '_token': $('input[name=_token]').val(),
            'role': option,
        },
        success: function(data) {
            $('.emailhide').remove();
            $('#receivers').append(data.response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("errorThrown: " + errorThrown);
        }

    });

});