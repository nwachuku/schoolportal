$(document).ready(function() {
    let URL_PATH = $("#URL_PATH").val() + '/get-emails';
    $.ajax({
        type: 'get',
        url: URL_PATH,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function(data) {
            $('#send_to').append(data.response);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("errorThrown: " + errorThrown);
        }

    });


    //Radio Changes
    $('.emailRecepient').on('change', function() {
        let option = this.value;
        let URL_PATH = $("#URL_PATH").val() + '/get-receipients';
        $.ajax({
            type: 'post',
            url: URL_PATH,
            data: {
                '_token': $('input[name=_token]').val(),
                'role': option,
            },
            success: function(data) {
                $('.emailhide').remove();
                $('#send_to option').each(function() {
                    $(this).remove();
                });
                $('#send_to').append(data.response);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("errorThrown: " + errorThrown);
            }

        });



    });


});