-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2018 at 02:33 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `title`, `description`, `course_id`, `user_id`, `created_at`, `updated_at`) VALUES
(9, 'Assignment 1', 'So the way to tell the difference between the Adapter pattern and the Facade pattern is that the Adapter wraps one class and the Facade may represent many classes?', 61, 38, '2018-03-04 02:28:12', '2018-03-04 02:28:12'),
(10, 'Assignment 2', 'The Facade design pattern is often used when a system is very complex or difficult to understand because the system has a large number of interdependent classes or its source code is unavailable', 62, 38, '2018-03-04 02:32:00', '2018-03-04 02:32:00'),
(11, 'Assignment 3', 'The Facade design pattern is often used when a system is very complex or difficult to understand because the system has a large number of interdependent classes or its source code is unavailable', 62, 38, '2018-03-04 02:32:39', '2018-03-04 02:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `auploads`
--

CREATE TABLE `auploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `assignment_id` int(10) UNSIGNED NOT NULL,
  `upload_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auploads`
--

INSERT INTO `auploads` (`id`, `assignment_id`, `upload_path`, `created_at`, `updated_at`) VALUES
(9, 9, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\assignments\\1520112492.jpg', '2018-03-04 02:28:12', '2018-03-04 02:28:12'),
(10, 10, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\assignments\\1520112720.txt', '2018-03-04 02:32:00', '2018-03-04 02:32:00'),
(11, 11, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\assignments\\1520112759.txt', '2018-03-04 02:32:39', '2018-03-04 02:32:39'),
(12, 11, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\assignments\\1520112759.txt', '2018-03-04 02:32:39', '2018-03-04 02:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` int(10) UNSIGNED NOT NULL,
  `chat_from` int(10) UNSIGNED NOT NULL,
  `chat_to` int(10) UNSIGNED NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `chat_from`, `chat_to`, `message`, `created_at`, `updated_at`) VALUES
(49, 37, 1, 'hi', '2018-03-04 04:32:51', '2018-03-04 04:32:51'),
(50, 37, 38, 'Hello dear', '2018-03-04 04:33:10', '2018-03-04 04:33:10'),
(51, 38, 37, 'Hi', '2018-03-04 04:33:54', '2018-03-04 04:33:54'),
(52, 1, 37, 'hey', '2018-03-04 07:23:15', '2018-03-04 07:23:15'),
(53, 37, 1, 'hey', '2018-03-04 07:43:34', '2018-03-04 07:43:34'),
(54, 37, 1, 'fgdtgdtdtyd', '2018-03-06 04:43:53', '2018-03-06 04:43:53');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `description`, `program_id`, `created_at`, `updated_at`) VALUES
(61, 'Advanced System Programming', 'Class Components have state, lifecycle methods, and properties while Functional Components only have properties. In this module, we will cover Functional Components while Class Components will be covered in Module 2.', 50, '2018-03-04 01:45:52', '2018-03-04 01:45:52'),
(62, 'Managing Employees', 'When you think of a factory, what comes to mind? For me, it\'s a place where things are created - that is, it\'s a centralized placed where things are produced. Later, the delivery of said products are done by the factory based on an order.', 50, '2018-03-04 01:46:10', '2018-03-04 01:46:10'),
(63, 'Software Engineering', 'When you think of a factory, what comes to mind? For me, it\'s a place where things are created - that is, it\'s a centralized placed where things are produced. Later, the delivery of said products are done by the factory based on an order.', 50, '2018-03-04 01:46:28', '2018-03-04 01:46:28'),
(64, 'Test', 'gdiuj.kfifui', 50, '2018-03-04 07:45:16', '2018-03-04 07:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `courses_students`
--

CREATE TABLE `courses_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses_students`
--

INSERT INTO `courses_students` (`id`, `course_id`, `student_id`, `year`, `created_at`, `updated_at`) VALUES
(6, 61, 37, '2018/2019', '2018-03-04 02:48:22', '2018-03-04 02:48:22'),
(7, 63, 37, '2018/2019', '2018-03-04 02:48:22', '2018-03-04 02:48:22'),
(10, 62, 37, '2018/2019', '2018-03-04 19:59:42', '2018-03-04 19:59:42');

-- --------------------------------------------------------

--
-- Table structure for table `course_lecturer`
--

CREATE TABLE `course_lecturer` (
  `id` int(10) UNSIGNED NOT NULL,
  `lecturer_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course_lecturer`
--

INSERT INTO `course_lecturer` (`id`, `lecturer_id`, `course_id`, `created_at`, `updated_at`) VALUES
(2, 2, 61, '2018-03-04 02:18:01', '2018-03-04 02:18:01'),
(3, 2, 62, '2018-03-04 02:18:01', '2018-03-04 02:18:01');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sender` int(10) UNSIGNED NOT NULL,
  `to` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `subject`, `body`, `attachment`, `priority`, `status`, `sender`, `to`, `created_at`, `updated_at`) VALUES
(6, 'autem', 'Voluptates alias possimus voluptas occaecati voluptas velit. Sapiente dolorem voluptate ut dolor sed vel blanditiis sed. Id aut rerum quam consectetur excepturi necessitatibus fugiat.', NULL, NULL, 1, 1, 1, '2018-02-27 19:17:31', '2018-02-27 19:17:31'),
(14, 'eos', 'Rerum error quia pariatur est minus voluptas sunt. Neque et dignissimos ab ut excepturi iure. Totam possimus commodi aliquid quidem eos.', NULL, NULL, 1, 1, 1, '2018-02-27 19:17:31', '2018-02-27 19:17:31'),
(17, 'accusantium', 'Perspiciatis dignissimos eos voluptas eos. Praesentium eos ut fugiat et. Provident unde sint est qui. Et vel et voluptatem nihil enim ut.', NULL, NULL, 1, 1, 1, '2018-02-27 19:17:31', '2018-02-27 19:17:31'),
(26, 'ipsum', 'Earum eveniet suscipit quos voluptas aut eum velit. In quas est et sunt sequi minus delectus.', NULL, NULL, 1, 1, 1, '2018-02-27 19:17:32', '2018-02-27 19:17:32'),
(28, 'quia', 'Voluptas inventore dicta distinctio eveniet blanditiis dolores. Molestiae iure sapiente rem. Exercitationem possimus sunt cumque aut consectetur sunt minus. Provident voluptas laboriosam saepe eos natus dolores excepturi voluptate. Non excepturi exercitationem quo sed placeat adipisci.', NULL, NULL, 1, 1, 1, '2018-02-27 19:17:32', '2018-02-27 19:17:32'),
(29, 'Deprecation Warning', '$user->name $user->email$user->name $user->email', NULL, NULL, 1, 38, 1, '2018-03-05 06:05:14', '2018-03-05 06:05:14'),
(30, 'Deprecation Warning', '$user->name $user->email$user->name $user->email', NULL, NULL, 1, 38, 37, '2018-03-05 06:05:14', '2018-03-05 06:05:14'),
(31, 'No Lecture Tomorrow', 'If you need to work with thousands of database records, consider using the chunk method. This method retrieves a small chunk of the results at a time and feeds each chunk into a Closure for processing. This method is very useful for writing Artisan commands that process thousands of records. For example, let\'s work with the entire users table in chunks of 100 records at a time:', NULL, NULL, 1, 38, 1, '2018-03-05 06:10:14', '2018-03-05 06:10:14'),
(32, 'No Lecture Tomorrow', 'If you need to work with thousands of database records, consider using the chunk method. This method retrieves a small chunk of the results at a time and feeds each chunk into a Closure for processing. This method is very useful for writing Artisan commands that process thousands of records. For example, let\'s work with the entire users table in chunks of 100 records at a time:', NULL, NULL, 1, 38, 37, '2018-03-05 06:10:14', '2018-03-05 06:10:14'),
(33, 'Inner Join Clause', 'The query builder may also be used to write join statements. To perform a basic \"inner join\", you may use the join method on a query builder instance. The first argument passed to the join method is the name of the table you need to join to, while the remaining arguments specify the column constraints for the join. Of course, as you can see, you can join to multiple tables in a single query:', 'C:\\xampp\\htdocs\\portal\\public\\uploads\\emails\\1520212312.jpg', NULL, 1, 38, 1, '2018-03-05 06:11:52', '2018-03-05 06:11:52');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator` int(10) UNSIGNED NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `date`, `time`, `description`, `color`, `creator`, `program_id`, `created_at`, `updated_at`) VALUES
(57, 'Stans birthday', '2018-03-14', '09:42:00', 'Class Components have state, lifecycle methods, and properties while Functional Components only have properties. In this module, we will cover Functional Components while Class Components will be covered in Module 2.', '#ff8040', 1, 50, '2018-03-04 05:40:57', '2018-03-04 05:40:57'),
(58, 'Master of Science Quiz', '2018-03-15', '08:53:00', 'A React Component is an independent reusable component that outputs a React Element based on its properties and state.', '#ff0000', 1, 50, '2018-03-04 05:52:23', '2018-03-04 05:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` int(10) UNSIGNED NOT NULL,
  `assignment1` int(11) NOT NULL DEFAULT '0',
  `assignment2` int(11) NOT NULL DEFAULT '0',
  `assignment3` int(11) NOT NULL DEFAULT '0',
  `quiz1` int(11) NOT NULL DEFAULT '0',
  `quiz2` int(11) NOT NULL DEFAULT '0',
  `quiz3` int(11) NOT NULL DEFAULT '0',
  `midterm` int(11) NOT NULL DEFAULT '0',
  `final` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '2',
  `student_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `lecturer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `assignment1`, `assignment2`, `assignment3`, `quiz1`, `quiz2`, `quiz3`, `midterm`, `final`, `total`, `status`, `student_id`, `course_id`, `lecturer_id`, `created_at`, `updated_at`) VALUES
(1, 3, 0, 6, 4, 0, 0, 30, 0, 43, 2, 37, 61, 38, '2018-03-04 23:42:08', '2018-03-05 05:31:13'),
(2, 10, 4, 4, 4, 0, 0, 0, 0, 22, 2, 37, 62, 38, '2018-03-05 04:50:10', '2018-03-05 05:19:08');

-- --------------------------------------------------------

--
-- Table structure for table `lecturercourses`
--

CREATE TABLE `lecturercourses` (
  `id` int(10) UNSIGNED NOT NULL,
  `lecturer_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lecturers`
--

CREATE TABLE `lecturers` (
  `id` int(10) UNSIGNED NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lecturers`
--

INSERT INTO `lecturers` (`id`, `gender`, `phone`, `country`, `degree`, `profile_image`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'male', '(328) 7426-2637', 'Afghanistan', 'Bachelors degree', 'C:\\xampp\\htdocs\\portal\\public\\uploads\\lecturers\\1520111880.jpeg', 38, '2018-03-04 02:18:00', '2018-03-04 02:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `title`, `description`, `course_id`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 'Materials 1', 'We distribute it in PDF & EPUB formats so you can get it onto your iPad, Kindle, or other portable device immediately after your purchase.', 61, 38, '2018-03-04 02:40:47', '2018-03-04 02:40:47'),
(4, 'Materials 4', 'This enables to work through a Facade object to minimize the dependencies on a subsystem.\r\nSee also the UML class and sequence diagram below.', 62, 38, '2018-03-04 02:41:30', '2018-03-04 02:41:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(52, '2014_10_12_000000_create_users_table', 1),
(53, '2014_10_12_100000_create_password_resets_table', 1),
(54, '2018_02_10_212447_create_email_table', 1),
(55, '2018_02_11_143303_create_program_table', 1),
(56, '2018_02_11_190844_create_event_table', 1),
(57, '2018_02_17_143526_create_course_table', 1),
(59, '2018_02_17_185552_create_materials_table', 1),
(60, '2018_02_17_190949_create_muploads_table', 1),
(61, '2018_02_17_205413_create_assignments_table', 1),
(62, '2018_02_17_215551_create_auploads_table', 1),
(63, '2018_02_18_005256_create_course_student_table', 1),
(64, '2018_02_20_151845_create_lecturer_table', 1),
(65, '2018_02_20_152225_create_lecturercourses_table', 1),
(66, '2018_02_21_154340_create_course_lecture_table', 1),
(68, '2018_02_26_174053_create_chat_table', 1),
(69, '2018_02_28_004853_create_settings_table', 2),
(70, '2018_02_24_03083fh5_create_exam_table', 3),
(71, '2018_02_17_1672013_create_students_table', 4),
(74, '2018_03_04_055202_create_submit_assignment_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `muploads`
--

CREATE TABLE `muploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `material_id` int(10) UNSIGNED NOT NULL,
  `upload_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `muploads`
--

INSERT INTO `muploads` (`id`, `material_id`, `upload_path`, `created_at`, `updated_at`) VALUES
(3, 3, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\materials\\1520113247.pdf', '2018-03-04 02:40:47', '2018-03-04 02:40:47'),
(4, 3, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\materials\\1520113247.txt', '2018-03-04 02:40:47', '2018-03-04 02:40:47'),
(5, 4, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\materials\\1520113290.pdf', '2018-03-04 02:41:30', '2018-03-04 02:41:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(50, 'Master of Science', 'Quia dolores expedita voluptatem delectus ipsam quis. Est neque et assumenda distinctio laboriosam consequatur et. Consequatur sequi aut veritatis ut et voluptate qui est. Quos ad illo aut assumenda eius.', '2018-02-27 19:17:40', '2018-03-03 23:53:31'),
(61, 'Master of Arts', 'We distribute it in PDF & EPUB formats so you can get it onto your iPad, Kindle, or other portable device immediately after your purchase.', '2018-03-04 05:04:51', '2018-03-04 07:22:25');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program_id` int(10) UNSIGNED NOT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `gender`, `phone`, `country`, `program_id`, `profile_image`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'male', '(876) 547-1223', 'Afghanistan', 50, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\profile\\1520102222.jpeg', 37, '2018-03-03 23:37:02', '2018-03-03 23:37:02');

-- --------------------------------------------------------

--
-- Table structure for table `submitassigments`
--

CREATE TABLE `submitassigments` (
  `id` int(10) UNSIGNED NOT NULL,
  `assignment_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `attachment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `submitassigments`
--

INSERT INTO `submitassigments` (`id`, `assignment_id`, `student_id`, `attachment`, `remark`, `created_at`, `updated_at`) VALUES
(1, 9, 37, 'C:\\xampp\\htdocs\\portal\\public\\uploads\\assignments\\1520144752.pdf', 'Just a note', '2018-03-04 11:25:52', '2018-03-04 11:25:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Georgianna Feeney', 'preilly@example.net', 'admin', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'nEBy0KQuqDN37QKw6i7GvaJBSIb3uc2kxGstL7pQetVx10dvyfQpnegrtZ63', '2018-02-27 19:17:26', '2018-02-27 19:17:26'),
(37, 'Nwachukwu Christian', 'nwachukwu16@hotmail.com', 'student', '$2y$10$nTHCQ24jubacyaKQFM8ITOUV1G71ruXGmdE9o5hXV1wm8ccpH8fb.', 'WeKJJlH9DFC6VtNSt1OqhB0HCRGYpOM8vYSHqctW4hwdnc1oqZuvQcl7Kq8r', '2018-03-03 23:37:01', '2018-03-03 23:37:01'),
(38, 'Edwards David', 'david@hotmail.com', 'lecturer', '$2y$10$.OFAUHI7YqopAEDElXIrFuKeaaCiV.p2tNuP.k0SNesGH9w37IGcG', 'SJ8LUFrPFaJdF9TGietq58lSkJCqZTZYWpykFILp6qSNefpIkhPiiKybdGkV', '2018-03-04 02:18:00', '2018-03-04 02:18:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignments_course_id_foreign` (`course_id`),
  ADD KEY `assignments_user_id_foreign` (`user_id`);

--
-- Indexes for table `auploads`
--
ALTER TABLE `auploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auploads_assignment_id_foreign` (`assignment_id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chats_chat_from_foreign` (`chat_from`),
  ADD KEY `chats_chat_to_foreign` (`chat_to`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_program_id_foreign` (`program_id`);

--
-- Indexes for table `courses_students`
--
ALTER TABLE `courses_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courses_students_course_id_foreign` (`course_id`),
  ADD KEY `courses_students_student_id_foreign` (`student_id`);

--
-- Indexes for table `course_lecturer`
--
ALTER TABLE `course_lecturer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_lecturer_course_id_foreign` (`course_id`),
  ADD KEY `course_lecturer_lecturer_id_foreign` (`lecturer_id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emails_sender_foreign` (`sender`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_creator_foreign` (`creator`),
  ADD KEY `events_program_id_foreign` (`program_id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exams_course_id_foreign` (`course_id`),
  ADD KEY `exams_student_id_foreign` (`student_id`),
  ADD KEY `exams_lecturer_id_foreign` (`lecturer_id`);

--
-- Indexes for table `lecturercourses`
--
ALTER TABLE `lecturercourses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lecturercourses_course_id_foreign` (`course_id`),
  ADD KEY `lecturercourses_lecturer_id_foreign` (`lecturer_id`);

--
-- Indexes for table `lecturers`
--
ALTER TABLE `lecturers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lecturers_user_id_foreign` (`user_id`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `materials_course_id_foreign` (`course_id`),
  ADD KEY `materials_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `muploads`
--
ALTER TABLE `muploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `muploads_material_id_foreign` (`material_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `students_user_id_foreign` (`user_id`),
  ADD KEY `students_program_id_foreign` (`program_id`);

--
-- Indexes for table `submitassigments`
--
ALTER TABLE `submitassigments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `submitassigments_assignment_id_foreign` (`assignment_id`),
  ADD KEY `submitassigments_student_id_foreign` (`student_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `auploads`
--
ALTER TABLE `auploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `courses_students`
--
ALTER TABLE `courses_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `course_lecturer`
--
ALTER TABLE `course_lecturer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lecturercourses`
--
ALTER TABLE `lecturercourses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lecturers`
--
ALTER TABLE `lecturers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `muploads`
--
ALTER TABLE `muploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `submitassigments`
--
ALTER TABLE `submitassigments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `assignments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `auploads`
--
ALTER TABLE `auploads`
  ADD CONSTRAINT `auploads_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`);

--
-- Constraints for table `chats`
--
ALTER TABLE `chats`
  ADD CONSTRAINT `chats_chat_from_foreign` FOREIGN KEY (`chat_from`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `chats_chat_to_foreign` FOREIGN KEY (`chat_to`) REFERENCES `users` (`id`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `courses_students`
--
ALTER TABLE `courses_students`
  ADD CONSTRAINT `courses_students_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `courses_students_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `course_lecturer`
--
ALTER TABLE `course_lecturer`
  ADD CONSTRAINT `course_lecturer_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `course_lecturer_lecturer_id_foreign` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturers` (`id`);

--
-- Constraints for table `emails`
--
ALTER TABLE `emails`
  ADD CONSTRAINT `emails_sender_foreign` FOREIGN KEY (`sender`) REFERENCES `users` (`id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_creator_foreign` FOREIGN KEY (`creator`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `events_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`);

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `exams_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `exams_lecturer_id_foreign` FOREIGN KEY (`lecturer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `exams_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `lecturercourses`
--
ALTER TABLE `lecturercourses`
  ADD CONSTRAINT `lecturercourses_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `lecturercourses_lecturer_id_foreign` FOREIGN KEY (`lecturer_id`) REFERENCES `lecturers` (`id`);

--
-- Constraints for table `lecturers`
--
ALTER TABLE `lecturers`
  ADD CONSTRAINT `lecturers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `materials`
--
ALTER TABLE `materials`
  ADD CONSTRAINT `materials_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `materials_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `muploads`
--
ALTER TABLE `muploads`
  ADD CONSTRAINT `muploads_material_id_foreign` FOREIGN KEY (`material_id`) REFERENCES `materials` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_program_id_foreign` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  ADD CONSTRAINT `students_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `submitassigments`
--
ALTER TABLE `submitassigments`
  ADD CONSTRAINT `submitassigments_assignment_id_foreign` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`),
  ADD CONSTRAINT `submitassigments_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
