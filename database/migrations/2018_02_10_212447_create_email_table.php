<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->longText('body');
            $table->string('attachment')->nullable();
            $table->string('mime_type')->nullable();
            $table->string('priority')->nullable();
            $table->integer('status')->default(1);
            $table->integer('sender')->unsigned();;
            $table->integer('to');
            $table->timestamps();

            $table->foreign('sender')->references('id')->on('users')->onDelete('cascade');;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
