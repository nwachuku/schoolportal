<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {

            $table->increments('id');
            $table->string('title');
            $table->date('date');
            $table->time('time');
            $table->longText('description');
            $table->string('color');
            $table->integer('creator')->unsigned();
            $table->integer('program_id')->unsigned();
            $table->timestamps();

            $table->foreign('creator')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('program_id')->references('id')->on('programs')->onDelete('cascade');;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
