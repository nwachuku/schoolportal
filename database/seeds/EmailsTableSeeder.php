<?php

use Illuminate\Database\Seeder;

class EmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Email::class, 30 )->create();
        factory(App\Program::class, 30 )->create();      
        factory(App\Event::class, 30 )->create();
        factory(App\Course::class, 30 )->create();



    }
}
