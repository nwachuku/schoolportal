<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{


    public function assignment(){
        return $this->belongsTo('App\Assignment');
    }
    //
    public function lecturer(){
        return $this->belongsTo('App\Lecturer');
    }

    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function student(){
        return $this->belongsTo('App\Student');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function exams(){
        return $this->hasMany('App\Exam');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y");
        return $newDate;
    }
}
