<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LecturerCourse extends Model
{
    //
    protected $table = 'lecturercourses';

    public function lecturer(){
        return $this->belongsTo('App\Lecturer');
    }

    public function course(){
        return $this->belongsTo('App\Course');
    }
    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }

}
