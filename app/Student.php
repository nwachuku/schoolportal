<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    protected $fillable = [
        'gender', 'phone', 'country', 'program_id', 'profile_image', 'user_id'
    ];

    public function getGenderAttribute($value)
    {
        return ucfirst($value);
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function program(){
        return $this->belongsTo('App\Program');
    }

   public function course_student(){
        return $this->hasMany('App\Course_Student');
    }

    public function exams(){
        return $this->hasMany('App\Exam');
    }

    

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }
}
