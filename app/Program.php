<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    //

    public function students(){
        return $this->hasOne('App\Student');
    }

    public function event(){
        return $this->hasMany('App\Event');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }
}
