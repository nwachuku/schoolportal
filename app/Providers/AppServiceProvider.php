<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Lecturer;
use App\Student;

use App\Http\Controllers\Helpers\HelperController;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
        // $users = User::all();
        // $lecturer = Student::where('user_id', 99)->get()->first();
        // View::share(['chat_students'=> $users, 'lecturer' => $lecturer]);
        $helperController = new HelperController;
        View::share(['util'=> $helperController]);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
