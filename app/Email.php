<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    //
    protected $fillable = [
        'subject', 'body', 'attachment', 'priority', 'status', 'sender', 'to'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }
 
}

