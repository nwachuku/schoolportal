<?php

namespace App\Http\Controllers;

use App\Lecturer;
use App\Course;
use App\User;
use App\CourseLecturer;
use App\LecturerCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class LecturerController extends Controller implements UserInterface
{

    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->authUser();
    }

     /**
     * Display a listing of the Lecturer.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all the lectures and courses
        //$lecturers = Lecturer::paginate(5);


          //Get all the resources that the view would need
          $lecturers = DB::table('users')
                        ->join( 'lecturers','users.id', '=', 'lecturers.user_id')
                        ->where('users.status', '1')
                        ->where('users.role', 'lecturer')
                        ->select('lecturers.user_id   as  user_id ' , 'lecturers.profile_image   as profile_image ' ,'lecturers.degree  as degree' , 'lecturers.country  as country' , 'lecturers.phone as phone' , 'lecturers.gender as gender' ,'lecturers.id as id' , 'users.name as name','users.email as email' ,'users.role as role' ,'users.status as status', 'users.id as user_id','users.created_at as created_at')
                        ->paginate(5);



        $courses   = Course::all();

        return view(
                    'lecturer.index', 
                    ['courses' => $courses , 'lecturers' => $lecturers ]
                );
    }

    /**
     * Store a newly created Lecturer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create a new user object
        $user = new User;

        //Configure the parameters for the queries 
        $user->name = $request->input('last_name')." ".$request->input('first_name');
        $user->email = $request->input('email');
        $user->role = 'lecturer';
        $user->password = bcrypt('secret');

        //save the user record
        $status = $user->save();

        //if record was saved successfully
        if($status)
        {

            //get the ID of the newly saved user
            $id  = $user->id;

            //variable to hold the attachment
            $attachment = null;

            $user_profile='';
            $UserUploadedImageURL='';
            
            //Check if the request has a file in it.
            if($request->hasFile($request->input('profile_image'))){

                $user_profile=time().'.'.$request->file('profile_image')->getClientOriginalExtension();
                $request->file('profile_image')->move(public_path('uploads\lecturers'), $user_profile);
                $attachment=$user_profile;

            }else{
                $attachment = null;
            }

            //Create the lecturer object
            $lecturer = new Lecturer;

            //Configure the query parameters
            $lecturer->gender        = $request->input('gender');
            $lecturer->phone         = $request->input('phone');
            $lecturer->country       = $request->input('country');
            $lecturer->degree        = $request->input('degree');
            $lecturer->profile_image = $attachment;
            $lecturer->user_id       = $id;
            
            //Determines if the courses were saved
            $handledcourse_status    = false;

            //If the lecturer was successfully saved
            if($lecturer->save()){

                //save the courses that the lecturer handles
                if(sizeof($request->courses) > 0){
                    foreach($request->input('courses') as $course){
                       
                        //create the CourseLecturer object
                        $handledcourse = new CourseLecturer;

                        //Configure the query parameters
                        $handledcourse->course_id   = $course;
                        $handledcourse->lecturer_id = $lecturer->id;

                        //Save the record
                        $handledcourse_status = $handledcourse->save();
                    }

                    //If the courses were saved
                    if($handledcourse_status){
                         $this->setSession('success', 'Lecturer details were successfully saved!');
                         return redirect()->back()->withInput();
                    }else{
                        $this->setSession('error', 'We could not save the details for the lecturer!');
                         return redirect()->back()->withInput();
                    }
                }
                //return view here...
                $this->setSession('success', 'Lecturer details were successfully saved!');
                         return redirect()->back()->withInput();
            }else{
                $this->setSession('error', 'We could not save the details for the lecturer!');
                         return redirect()->back()->withInput();
            }

            if($lecturer->id){
                return redirect()->route('lectures.index');
            }else{
                return redirect()->back()->withInput();
            }
        }else{
            $this->setSession('error', 'We could not save the details for the lecturer!');
                         return redirect()->back()->withInput();
        }
        
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $lecturer_id = Lecturer::where('user_id', $id)->value('id');

        $user->name = $request->input('last_name')." ".$request->input('first_name');
        $user->email = $request->input('email');

        if($user->update()){

            //variable to hold the attachment
            $attachment = null;

            $user_profile='';
            $UserUploadedImageURL='';
            
            //Check if the request has a file in it.
            if($request->hasFile($request->input('profile_image'))){
                $user_profile=time().'.'.$request->file('profile_image')->getClientOriginalExtension();
                $request->file('profile_image')->move(public_path('uploads\lecturers'), $user_profile);
                $attachment=$user_profile;
            }else{
                $attachment = null;
            }

            //Create the lecturer object
            $lecturer = Lecturer::where('user_id', $id)->first();

            //Configure the query parameters
            $lecturer->gender        = $request->input('gender');
            $lecturer->phone         = $request->input('phone');
            $lecturer->country       = $request->input('country');
            $lecturer->degree        = $request->input('degree');
            $lecturer->profile_image = $attachment;
            
            //Determines if the courses were saved
            $handledcourse_status    = false;

            //If the lecturer was successfully saved
            if($lecturer->update()){

                
                //delete the courses that the lecturer handles.
                $lecturer_c = CourseLecturer::where('lecturer_id', $lecturer_id)->first();

                if($lecturer_c){

                    if($lecturer_c->delete()){

             
                        //save the courses that the lecturer handles
                        if(sizeof($request->courses) > 0){

                            foreach($request->input('courses') as $course){

                                //create the CourseLecturer object
                                $handledcourse = new CourseLecturer;

                                //Configure the query parameters
                                $handledcourse->course_id   = $course;
                                $handledcourse->lecturer_id = $lecturer->id;

                                //Update the record
                                $handledcourse_status = $handledcourse->save();
                            }

                            //If the courses were saved
                            if($handledcourse_status){
                                $this->setSession('success', 'Lecturer details were successfully saved!');
                                return redirect()->back()->withInput();
                            }else{
                                $this->setSession('error', 'We could not save the details for the lecturer!');
                                return redirect()->back()->withInput();
                            }
                        }
                        //return view here...
                        $this->setSession('success', 'Lecturer details were successfully updated!');
                        return redirect()->back()->withInput();

                }else{
                    $this->setSession('error', 'We could not update the details for the lecturer!');
                    return redirect()->back()->withInput();

                }

                
                }else{
                    //save the courses that the lecturer handles
                    if(sizeof($request->courses) > 0){

                        foreach($request->input('courses') as $course){

                            //create the CourseLecturer object
                            $handledcourse = new CourseLecturer;

                            //Configure the query parameters
                            $handledcourse->course_id   = $course;
                            $handledcourse->lecturer_id = $lecturer->id;

                            //Update the record
                            $handledcourse_status = $handledcourse->save();
                        }

                        //If the courses were saved
                        if($handledcourse_status){
                            $this->setSession('success', 'Lecturer details were successfully saved!');
                            return redirect()->back()->withInput();
                        }else{
                            $this->setSession('error', 'We could not save the details for the lecturer!');
                            return redirect()->back()->withInput();
                        }
                    }
                }

                }else{
                    $this->setSession('error', 'We could not update the details for the lecturer!');
                    return redirect()->back()->withInput();
                }


        }else{
             $this->setSession('error', 'We could not update the details for the user!');
             return redirect()->back()->withInput();

        }

    }



    /**
     * Show the form for editing the specified Lecturer.
     *
     * @param  \App\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function edit(Lecturer $lecturer)
    {

        //get the user Id
        $user_id = $lecturer->user_id;

        //write a join query to retrieve the lecturer information from users and lecturers tables
        $lecturer_info = DB::table('users')
                        ->join('lecturers', 'users.id', '=', 'lecturers.user_id')
                        ->where('users.id', '=', $user_id)
                        ->first();

        $lecturer_courses =  CourseLecturer::where('lecturer_id', $lecturer_info->id)->pluck('course_id')->toArray();;

         //Get all the resources that the view would need
         $lecturers = DB::table('users')
         ->join( 'lecturers','users.id', '=', 'lecturers.user_id')
         ->where('users.status', '1')
         ->where('users.role', 'lecturer')
         ->select('lecturers.user_id   as  user_id ' , 'lecturers.profile_image   as profile_image ' ,'lecturers.degree  as degree' , 'lecturers.country  as country' , 'lecturers.phone as phone' , 'lecturers.gender as gender' ,'lecturers.id as id' , 'users.name as name','users.email as email' ,'users.role as role' ,'users.status as status', 'users.id as user_id','users.created_at as created_at')
         ->paginate(5);

        
        $courses   = Course::all();

        return view('lecturer.edit', 
                  [
                      'courses' => $courses ,
                      'lecturers' => $lecturers,
                      'lecturer_info' => $lecturer_info,
                      'lecturer_courses' => $lecturer_courses
                      ]
        );
    
    }



     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Lecturer $lecturer)
    {
        //
        $user = User::find($lecturer->user_id);

        
        
        $course_lecturer = $users = DB::table('course_lecturer')->where('lecturer_id', $this->userId())->get();

     
        $img = false;
        if($lecturer->profile_image){
            $img = "uploads\lecturers\\".$lecturer->profile_image;
        }else{
            $img = false;
        }
        return view('lecturer.show', 
        
                [
                    'user' => $user,
                    'lecturer' => $lecturer,
                    'img' => $img,
                    'course_lecturer' => $course_lecturer
                ]
            );
    }


    public function destroy($id)
    {

        $user = Lecturer::where("id", $id)->value('user_id');
  
        $user_instance = User::find($user);
 

        if($user_instance->delete()){
            $this->setSession('success', 'Lecturer was successfully deleted!');
            return redirect()->back();
        }

    }

}
