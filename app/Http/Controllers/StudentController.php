<?php

namespace App\Http\Controllers;

use App\Student;
use App\Program;
use App\Course;
use App\User;
use App\Chat;
use App\Email;
use App\Exam;
use App\SubmitAssignment;
use App\Course_Student;
use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\UserInterface;

class StudentController extends Controller implements UserInterface
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $courses = Course::orderby('title')->get();


        $students = Student::orderby('id')->paginate(5);


        $programs = Program::orderby('title')->get();
        return view('students.index', ['students'=> $students , 'courses' => $courses, 'programs' => $programs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


                    //Validating the user inputs
                    $this->validate($request, [
                        'profile_image'  => 'max:30|image',
                    ]);


                    $user = new User;

                    $user->name = $request->input('last_name')." ".$request->input('first_name');
                    $user->email = $request->input('email');
                    $user->role = 'student';
                    $user->password = bcrypt('secret');

                    $status = $user->save();

                    if($status)
                    {
                        $id  = $user->id;

                        $attachment = null;

                        //VALIDATE THE REQUEST INPUTS
                        $user_profile='';
                        $UserUploadedImageURL='';
                        if($request->hasFile($request->input('profile_image'))){
                            $user_profile=time().'.'.$request->file('profile_image')->getClientOriginalExtension();
                            $request->file('profile_image')->move(public_path('uploads\profile'), $user_profile);
                            $attachment= $user_profile;
                        }else{
                            $attachment = null;
                        }

                        $student = new Student;

                        $student->gender     = $request->input('gender');
                        $student->phone      = $request->input('phone');
                        $student->country    = $request->input('country');
                        $student->program_id = $request->input('program');
                        $student->profile_image = $attachment;
                        $student->user_id = $id;

                        $student->save();
                        $student->id;

                        if($student->id){
                            $this->setSession('success', 'Student details were successfully saved!');
                            return redirect()->route('students.index');
                        }else{
                            $this->setSession('error', 'We could not save the details for the student!');
                            return redirect()->back()->withInput();
                        }
                    }
 
                    return redirect()->back()->withInput();
         }


          /**
     * Show the form for editing the specified Lecturer.
     *
     * @param  \App\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {

        $courses = Course::orderby('title')->get();
        $students = Student::orderby('id')->paginate(5);
        $programs = Program::orderby('title')->get();
        $student_info = Student::find($student->id);
        $user_info = User::find($student_info->user_id);

            return view('students.edit', 
                        [
                            'students'=> $students ,
                            'courses' => $courses, 
                            'programs' => $programs,
                            'student_info' => $student_info,
                            'user_info' => $user_info
                        ]
        );
    
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
        $user = User::find($student->user_id);
        $img = false;
        if($student->profile_image){
            $img = "uploads\profile\\".$student->profile_image;
        }else{
            $img = false;
        }
        return view('students.show', 
        
                [
                    'user' => $user,
                    'student' => $student,
                    'img' => $img
                ]
            );
    }




    public function update(Request $request, $id)
    {

        $user = User::find($id);

        $user->name = $request->input('last_name')." ".$request->input('first_name');
        $user->email = $request->input('email');
        $user->role = 'student';
        $user->password = bcrypt('secret');

        $status = $user->update();


        if($status){

            $id  = Student::where('user_id', $id)->value('id');

            $attachment = null;

            //VALIDATE THE REQUEST INPUTS
            $user_profile='';
            $UserUploadedImageURL='';
            if($request->hasFile($request->input('profile_image'))){
                $user_profile=time().'.'.$request->file('profile_image')->getClientOriginalExtension();
                $request->file('profile_image')->move(public_path('uploads\profile'), $user_profile);
                $attachment= user_profile;
            }else{
                $attachment = null;
            }

            $student = Student::find($id);

            $student->gender     = $request->input('gender');
            $student->phone      = $request->input('phone');
            $student->country    = $request->input('country');
            $student->program_id = $request->input('program');
            $student->profile_image = $attachment;
            $student->user_id = $id;

            $student_stat = $student->update();

            if( $student_stat){
                $this->setSession('success', 'Student details were successfully updated!');
                return redirect()->back()->withInput();
            }else{
                $this->setSession('error', 'We could not update the details for the student!');
                return redirect()->back()->withInput();
            }

        }else{
            $this->setSession('error', 'We could not updated the details for the student!');
            return redirect()->back()->withInput();
        }
    }

    
    public function destroy($id)
    {

        $user = Student::where("id", $id)->value('user_id');
  

        $user_id  = $user;

        $delete_chat = Chat::where('chat_from', $user_id)->orWhere('chat_to', $user_id)->delete();
        $delete_course_reg = Course_Student::where('student_id', $user_id)->delete();
        $delete_email = Email::where('sender', $user_id)->orWhere('to', $user_id)->delete();
        $delete_exam = Exam::where('student_id', $user_id)->delete(); 
        $delete_submitted_assignment = SubmitAssignment::where('student_id', $user_id)->delete();
        $delete_student = Student::where('user_id', $user_id)->delete();
        $delete_user = User::where('id', $user_id)->delete();
        if($delete_user){
            $this->setSession('success', 'Student was successfully deleted!');
            return redirect()->route('students.index');        }

    }
}
