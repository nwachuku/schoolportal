<?php

namespace App\Http\Controllers;

use App\Course_Student;
use App\Course;
use App\Program;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
/**
* CourseStudentController is a class for handling all the tasks related to course that the student registered for.
*
* @author   Stanley
* @author   Ben
* @date     3/3/2018
*/

class CourseStudentController extends Controller
{

    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->authUser();
    }

    /**
     * Display a listing of the CourseStudent.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all the courses that the students registered for.
       $records = Course_Student::where('student_id', $this->userId())->paginate(5);
       $courses = Course::orderby('title')->get();
       return view('courses.register', ['courses' => $courses, 'records' => $records]);

    }

    
    /**
     * Store a newly created CourseStudent in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //loop through all the courses that the user wants to register for
        foreach($request->courses as $course){

            //Check if the student has registered for the course.
            $check = Course_Student::where('course_id', $course)
                            ->where('student_id', $this->userId())
                            ->get();

            //If the record is found...return with error message
            if(count($check) > 0){
                $this->setSession('error', 'You have already registered for this course');
                return redirect()->back()->withInput();
            }
            
            //Create the  object to handle the course registration
            $csobject = new Course_Student;

            //configure the query parameters
            $csobject->course_id =  $course;
            $csobject->year =  $request->year;
            $csobject->student_id =   $this->userId();

            //save the record
            $csobject->save();

        }

        $this->setSession('success', 'You have successfully registered for this course(s)');

        return redirect()->route('cs.index');


    }

    /**
     * Remove the specified CourseStudent from storage.
     *
     * @param  \App\Course_Student  $course_Student
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find the specific record
        $course_Student = Course_Student::find($id);

        //delete the record
        if($course_Student->delete()){
            $this->setSession('success', 'You have successfully deleted this course');
            return redirect()->route('cs.index');
        }

        $this->setSession('error', 'We could not delete this course');
        return redirect()->route('cs.index');

    }
}
