<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
Use App\Student;
use App\Course_Student;
use Illuminate\Http\Request;

/**
* StudentAjaxController is a class for handling all the ajax tasks related to students
*
* @author   Stanley
* @author   Ben
* @date     3/3/2018
*/
class StudentAjaxController extends Controller
{

    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->authUser();
    }
    

    //
    public function test(Request $request){

        $students = DB::table('courses_students')
        ->crossJoin('students', 'students.id', '!=', 'courses_students.student_id')
        ->join('users', 'users.id', '=', 'students.user_id')
        ->where('courses_students.course_id', '=', $request->id)
        ->distinct()
        ->get();
        $response = null;
        if(count($students) > 0 ){
            $response.= "<label class='student-hide'>Student</label>";
            $response.= '<select class="form-control student-hide" name="student" id="student" data-parsley-required>';
           
            foreach($students as $student){
              $response.= "<option value=".$student->id.">".$student->name."</option>";
            }

            $response.="</select>";

        }else{

            $response.= '<p class="student-hide">No student is registered yet</p>';
        }

        


        
        if ($request->isMethod('post')){ 
            return response()->json(['response' => $response]); 
        }
    }
}
