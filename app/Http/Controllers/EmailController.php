<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Emailcondition;
 use App\Http\Requests;


use Session;
use App\User;
use App\Email;

class EmailController extends Controller
{
    protected $email_inbox_count = null;
    protected $email_sent_count = null;
    protected $logged_in_id = null;

    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->middleware('auth');

        

        //GET INBOX COUNT
        $this->email_inbox_count = count(Email::where('to', $this->logged_in_id)->get());

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $emails = Email::where('to', Auth::id())->where('status', '1')->simplePaginate(15);
        //
        return view('email.email', [
                    'emails' => $emails, 
                    'folder' => 'inbox',
                    'inbox_count' => $this->email_inbox_count,
                    'inbox_count' => $this->inbox_emails(),
                    'sent_count' => $this->sent_emails(),
                    'trash_count' => $this->trash_emails(),
                    'starred_count' => $this->starred_count()


                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users = User::where('id','!=', Auth::id())->get();
        return view('email.create', [
         'users' => $users,
         'folder' => 'none',  
         'inbox_count' => $this->inbox_emails(),
         'sent_count' => $this->sent_emails(),
         'trash_count' => $this->trash_emails(),
         'starred_count' => $this->starred_count()

         ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


         //IF THE USER IS LOGGED IN THEN SEND THE EMAIL
        if(Auth::check()){
 
            $attachment = null;
            $mime_type = null;

            //VALIDATE THE REQUEST INPUTS
            if ($request->file('attachment') == null) {
                $attachment = null;
            }else{

                $emailsattachment=time().'.'.$request->file('attachment')->getClientOriginalExtension();
                $attachment=$request->file('attachment')->move(public_path('uploads\emails'), $emailsattachment);
                $mime_type = $request->file('attachment')->getClientMimeType();
             }

             //SEND EMAIL FOR ALL THE RECEIPIENTS
            foreach ($request->to as $receipent){

                $email = new Email;

                $email->subject      = $request->input('subject');
                $email->body         = $request->input('body');
                $email->attachment   = $attachment;
                $email->mime_type    = $mime_type;
                $email->sender       = Auth::user()->id;
                $email->to           =  $receipent;

                //SEND EMAIL TO THE USER
                $email_status = $email->save();
            }

            if($email_status){
                 return redirect()->route('emails.index');
            }
        }else{
            return redirect()->back()->withInput();
        }

     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //$delete_email =  Email::where('id',$id)->delete();

        $email = Email::find($id);

        $email->status = 3;
        
        $email->save();

        if($email){
            return redirect()->back()->withInput();
        } 

    }


    public function starred($id){
        $email = Email::find($id);

        $email->status = 4;

        $email->save();

        return redirect()->back()->withInput();

    }

    public function unstarred($id){
        $email = Email::find($id);

        $email->status = 1;

        $email->save();

        return redirect()->back()->withInput();

    }


    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        //$delete_email =  Email::where('id',$id)->delete();

        $email = Email::find($id);
        $to = $email->to;
        $sender = $email->sender;

        $email->status = 1;

        $email->save();

        if($email){
            //DETERMINE WHERE TO RESTORE THE EMAIL
            if($sender == Auth::id() && $to == Auth::id()){
                return redirect()->route('emails.index');
            }
            elseif($sender == Auth::id()){
                return redirect()->route('email.sent');
            }elseif($to == Auth::id()){
                return redirect()->route('emails.index');
            }
        } 
    }

    public function sent(){
        $emails = Email::where('sender', Auth::id())->where('status', '1')->simplePaginate(15);
        return view('email.email', [
                     'emails' => $emails,
                     'folder' => 'sent',
                     'inbox_count' => $this->inbox_emails(),
                     'sent_count' => $this->sent_emails(),
                     'trash_count' => $this->trash_emails(),
                     'starred_count' => $this->starred_count()

               ]);
    }

    public function starred_emails(){
        $emails = Email::where('to', Auth::id())->where('status', '4')->simplePaginate(15);
        return view('email.email', [
                     'emails' => $emails,
                     'folder' => 'starred',
                     'inbox_count' => $this->inbox_emails(),
                     'sent_count' => $this->sent_emails(),
                     'trash_count' => $this->trash_emails(),
                     'starred_count' => $this->starred_count(),
                     'starred_count' => $this->starred_count()

               ]);
    }

    public function trash(){

       $emails = Email::where('status', '3')
                        ->where(function ($query) {
                            $query->where('sender', $this->userId())
                                  ->orWhere('to', $this->userId());
                            })
                        ->simplePaginate(15);
       
 
        //$emails = DB::select('select * from emails where (`sender` = ? OR `to` = ?) AND `status` = ?', [Auth::id() , Auth::id(),  '3']);
   
        return view('email.email', [
                     'emails' => $emails,
                     'folder' => 'trash',
                     'inbox_count' => $this->inbox_emails(),
                     'sent_count' => $this->sent_emails(),
                     'trash_count' => $this->trash_emails(),
                     'starred_count' => $this->starred_count()

               ]);

               
    }

    public function read($id){

        //check if we already saved the data.
        $find    = Emailcondition::where('email_id', $id)->where('user_id', $this->userId())->first();
        $isInbox = Email::where('id', $id)->value('status');
         if(!$find && (int)$isInbox == 1){
            
            //insert data into the emailconditions table
            $save_email_status = new Emailcondition;

            $save_email_status->email_id = $id;
            $save_email_status->user_id = $this->userId();
            
            $save_email_status->save();
        }

        

        $email  = Email::where('id',$id)->get()->first();
        return view('email.view', [
            'email' => $email, 
         'folder' => 'none',  
         'inbox_count' => $this->inbox_emails(),
         'sent_count' => $this->sent_emails(),
         'trash_count' => $this->trash_emails(),
         'starred_count' => $this->starred_count()
        ]);
    }

    private function sent_emails(){
        if(Auth::check()){

            $this->logged_in_id = intval(Auth::id());
            //GET INBOX COUNT
            return $this->email_sent_count =  Email::where('sender', $this->logged_in_id )->where('status', '1')->count();
           } 
    }

    private function inbox_emails(){
        //get all read email Id:
       $read_emails = Emailcondition::where('user_id', $this->userId())->pluck('email_id')->toArray();
 
       if(Auth::check()){

            $this->logged_in_id = intval(Auth::id());
            //GET INBOX COUNT
            return $this->email_sent_count =  Email::where('to', $this->logged_in_id )
                                            ->where('status', '1')
                                            ->whereNotIn('id',$read_emails)
                                            ->count();
           
        } 
    }

    private function trash_emails(){
        if(Auth::check()){

            $this->logged_in_id = intval(Auth::id());

            return $users = count(DB::select('select * from emails where (`sender` = ? OR `to` = ?) AND `status` = ?', [$this->logged_in_id , $this->logged_in_id,  '3']));

        } 
    }


    public function starred_count(){
         return $emails = Email::where('to', Auth::id())->where('status', '4')->count();
        
    }

    public function ajaxGetAllReceipients(){

        $users = User::all();
        $response = null;
        
        if(count($users) > 0 ){
           // $response.='<label class="emailhide" for="exampleFormControlSelect2">Receipient</label>';
            foreach($users as $user){
                $response.='<option value="'.$user->id.'">'.$user->name.' - '.$user->email.'</option>';
            }
        }else{
            $response.= '<p class="emailhide">No receipient was found/p>';
        }


        return response()->json(['response' => $response]); 
    }

    public function ajaxGetReceipients(Request $request){
        $users = User::where('role', $request->role)->get();
        $response = null;

        if(count($users) > 0 ){
            //$response.='<label class="emailhide" for="to">Receipient</label>';
            //$response.='<select multiple="" name="to[]" class="form-control emailhide">';
            foreach($users as $user){
                $response.='<option value="'.$user->id.'">'.$user->name.' - '.$user->email.'</option>';
            }
           // $response.='</select>';
        }else{
            $response.= '<p class="emailhide">No receipient was found</p>';
        }

        return response()->json(['response' => $response]); 

    }

}
