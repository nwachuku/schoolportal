<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

use App\Event;
use App\Course;
use App\Program;
use App\Student;
/**
* EventController is a class for handling all the tasks related to events
*
* @author   Stanley
* @author   Ben
* @date     3/3/2018
*/


class EventController extends Controller
{
    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->authUser();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Get all the necessary records that the view would use 
        $events   = DB::table('events')->orderby('date')->paginate(5);
        $courses  = Course::all();
        $programs = Program::all();

        //Check the role before displaying the view
        if( $this->userRole() == 'admin' || $this->userRole() == 'lecturer'){
            //return the view that allows the user to create a new event
            return view('event.create',
                                [
                                'events'=>$events, 
                                'programs' => $programs
                                ]
                        );
        }else{
            //Get the program that the user is registered in...
            $program = DB::table('students')->where('user_id',$this->userId())->select('program_id')->value('program_id');
           //Get the event that are related to the user
            $events   = DB::table('events')->where('program_id',$program)->orWhere('program_id',1)->where('date', '>', now())->orderby('date')->paginate(5);
            return view('event.index', ['events'=>$events, 'courses' => $courses , 'programs' => $programs]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Event::orderby('date')->get()->paginate(5);
        return view('event.create', ['events'=>$events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $event = new Event;

            $event->title        = $request->input('title');
            $event->date         = $request->input('date');
            $event->time         = $request->input('time');
            $event->description  = $request->input('desc');
            $event->program_id   = $request->program;
            $event->color        = $request->input('priority');
            $event->creator      = $this->userId();

            $event_status =  $event->save();

            if($event_status){
                $this->setSession('success', 'Event was successfully created!');
                return redirect()->route('events.index');
            }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $event = Event::find($id);
        $programs = Program::all();
        return view('event.edit', ['event' => $event, 'programs' => $programs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //Configure the query parameters
            $args = [
                'title'           => $request->input('title'),
                'date'            => $request->input('date'),
                'time'            => $request->input('time'),
                'description'     => $request->input('desc'),
                'program_id'      => $request->input('program'),
                'color'           => $request->input('priority'),
            ];

            //Find the specific event
            $event = Event::find($id);

            //update that event
            $update_status = $event->update($args);

            
            if($update_status){

                $this->setSession('success', 'Event was successfully updated!');
                return redirect()->route('events.index');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Find the specific event
        $event = Event::find($id);

        //Delete the event
        $status = $event->delete();

        if($status){
            $this->setSession('success', 'Event was successfully deleted!');
            return redirect()->route('events.index');


        }else{
            $this->setSession('error', 'Event was not successfully deleted!');
            return redirect()->route('events.index');

            
        }  
    }


    
}
