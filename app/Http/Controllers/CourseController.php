<?php

namespace App\Http\Controllers;

use App\Course;
use App\Program;
use App\Aupload;
use App\Assignment;
use App\Material;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

/**
* CourseController is a class for handling all the tasks related to courses
*
* @author   Stanley
* @author   Ben
* @date     3/3/2018
*/

class CourseController extends Controller
{

    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->authUser();
    }

    /**
     * Display a listing of the course.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all the courses
        $courses = DB::table('courses')->orderby('title')->paginate(5);

        //Get all the programs
        $programs = Program::all();

        return view('courses.index', ['courses' => $courses, 'programs' => $programs]);
    }

    /**
     * Store a newly created course in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        if($this->loggedIn()){
             //create a new course instance
            $course = new Course;

            //configure the query parameters
            $course->title = $request->input('title');
            $course->description = $request->input('desc');
            $course->program_id = $request->input('program');
           
            //Save the new course record
            $status = $course->save();

            return  redirect()->route('courses.index');

        }else{
            //set a session notification
            $this->setSession('warning', 'You must be logged in to perform this action');
            return redirect()->route('home');

        }
       

    }

    /**
     * Show the form for editing the specified course.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
            if($this->loggedIn()){
                //Get all the courses: display 5 courses per page
                $courses  = Course::orderby('title')->paginate(5);
                //get all the programs
                $programs = Program::all();
                return view('courses.edit', ['course' => $course, 'courses' => $courses, 'programs' => $programs]);
            }else{
                //set a session notification
                $this->setSession('warning', 'You must be logged in to perform this action');
                return redirect()->route('home');
            }
    }

    /**
     * Update the specified course in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        if($this->loggedIn()){
            //Configure the query parameters before you update the record
            $course->title = $request->input('title');
            $course->description = $request->input('desc');
            $course->program_id = $request->input('program');
            //update the record
            $status = $course->update();

            if($status)
                return  redirect()->route('courses.index');
            else
                return  redirect()->route('courses.edit');
        }else{
            //set a session notification
            $this->setSession('warning', 'You must be logged in to perform this action');
            return redirect()->route('home');
        }

    }

    /**
     * Remove the specified course from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {

        // $delete_upload = Aupload::where('course_id', $course->id)->delete();



        DB::table('auploads')
            ->join('assignments', 'assignments.id', '=', 'auploads.assignment_id')
            ->where('assignments.course_id', '=', $course->id)
            ->delete();

        DB::table('submitassigments')
        ->join('assignments', 'assignments.id', '=', 'submitassigments.assignment_id')
        ->where('assignments.course_id', '=', $course->id)
        ->delete();
            
     
        $delete_assignment = Assignment::where('course_id', $course->id)->delete();


        DB::table('muploads')
        ->join('materials', 'materials.id', '=', 'muploads.material_id')
        ->where('materials.course_id', '=', $course->id)
        ->delete();
 
         Material::where('course_id', $course->id)->delete();



        DB::table('courses_students')->where('course_id', '=', $course->id)
        ->delete();

        DB::table('course_lecturer')->where('course_id', '=', $course->id)
        ->delete();

        DB::table('exams')->where('course_id', '=', $course->id)
        ->delete();

         //delete the course
        // if($course->delete()){
        //      return redirect()->route('courses.index');
        // }
        redirect()->route('courses.index');
    }

    public function register(){
        return view('courses.register');
    }
}
