<?php

namespace App\Http\Controllers;

use App\SubmitAssignment;
use App\Assignment;
use App\Course;
use App\Course_Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SubmitAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


              //select all the assignments in the course that the student registered in
            $submissions = SubmitAssignment::where('student_id', $this->userId())->paginate(5);
      
       
        return view('assignment.submissions', ['submissions' => $submissions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create a new instance of the object
        $submitAssignment = new SubmitAssignment;
        $attachment = null;

        if ($request->hasFile('attachment')) {
            $attachment_file=time().'.'.$request->file('attachment')->getClientOriginalExtension();
            $attachment=$request->file('attachment')->move(public_path('uploads\assignments'), $attachment_file);
        }


        //Configure query parameters
        $submitAssignment->assignment_id = $request->id;
        $submitAssignment->student_id    = $this->userId();
        $submitAssignment->attachment    = $attachment;
        $submitAssignment->remark        = $request->remark;

        //Save the new submission
        $submitAssignment->save();

        return redirect()->route('submissions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubmitAssignment  $submitAssignment
     * @return \Illuminate\Http\Response
     */
    public function show($submitAssignment)
    {
            $submissions =  DB::table('assignments')
                            ->join('submitassigments', 'assignments.id', '=', 'submitassigments.assignment_id')
                            ->join('users', 'users.id', '=', 'submitassigments.student_id')
                            ->where('assignments.user_id', '=', $this->userId())
                            ->where('submitassigments.assignment_id', $submitAssignment)
                            ->orderBy('submitassigments.created_at', 'desc')
                            ->select('assignments.title as title' , 'submitassigments.attachment  as attachment','submitassigments.id as id','submitassigments.remark as remark' ,'submitassigments.created_at as created_at', 'users.name as name')
                            ->paginate(5);

             return view('assignment.submissions', ['submissions' => $submissions]);

     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubmitAssignment  $submitAssignment
     * @return \Illuminate\Http\Response
     */
    public function edit(SubmitAssignment $submitAssignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubmitAssignment  $submitAssignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubmitAssignment $submitAssignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubmitAssignment  $submitAssignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubmitAssignment $submitAssignment)
    {
        //
    }


    public function submitShow($assignment){
     
        //select all the assignments in the course that the student registered in
        $submissions = SubmitAssignment::where('student_id', $this->userId())->paginate(5);


        //select all the assignments in the course that the student registered in
        $single_assignment = Assignment::where('id', $assignment)->get()->first();

       $submitted = SubmitAssignment::where('student_id' , $this->userId())->where('assignment_id', $assignment)->count();
       $submitted = ($submitted > 0)? 'disabled' : '';
        return view(
                    'assignment.submit',
                    ['submissions'=>$submissions, 'assignment' => $single_assignment, 'submitted' => $submitted]
                   );
}
}
