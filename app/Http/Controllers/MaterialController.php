<?php

namespace App\Http\Controllers;

use App\Material;
use App\Course;
use App\Mupload;
use App\Course_Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        if($this->userRole() != 'student'){

            $materials = Material::paginate(5);

            //Get all the courses that the lecturer is registered into
            $courses = DB::table('course_lecturer')
                            ->join('courses', 'courses.id', '=', 'course_lecturer.course_id')
                            ->where('course_lecturer.lecturer_id', '=', $this->lecturerId())
                            ->select('courses.id as id' , 'courses.title as title')
                            ->get();
        }else{

             //create an array to hold the result of the courses that the student registered in
             $courses = Course_Student::where('student_id', $this->userId())->pluck('course_id');

              //select all the materials in the course that the student registered in
              $materials = Material::whereIn('course_id', $courses)->paginate(5);

        }



        


        return view('materials.index', ['courses' => $courses, 'materials' => $materials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       
        
        $material = new Material;

        $material->title        = $request->input('title');
        $material->description  = $request->input('desc');
        $material->course_id    = $request->input('course');
        $material->user_id      = Auth::user()->id;


        $material->save();

        if($material->id){

            $attachment = null;

            //VALIDATE THAT THE MATERIALS WERE ATTACHED
            if ($request->hasFile('attachments')) {
                
                foreach($request->file('attachments') as $attach){
                   
                    $mupload = new Mupload;

                    $filename='';
                    $UserUploadedImageURL='';
                    $filename = time().'.'.$attach->getClientOriginalExtension();
                    $attachment = $attach->move(public_path('uploads\materials'), $filename);
                  
                    //insert uploads here...
                    $mupload->material_id =  $material->id;
                    $mupload->upload_path =  $attachment;

                    $mupload->save(); 
                } 

                return redirect()->route('materials.index');


            }

        }else{
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function show(Material $material)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function edit(Material $material)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Material $material)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Material $material)
    {

        $mupload = Mupload::where('material_id', $material->id)->delete();
    
        if($mupload){
            //
            if($material->delete()){
                return redirect()->route('materials.index');
            }
        }
        

        redirect()->route('materials.index');
    }
}
