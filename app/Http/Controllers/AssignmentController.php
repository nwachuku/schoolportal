<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Aupload;
use App\Course;
use App\Course_Student;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;



/**
* AssignmentController is a class for handling all the tasks related to assignments
*
* @author   Stanley
* @author   Ben
* @date     3/3/2018
*/


class AssignmentController extends Controller
{


    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->authUser();
    }
    
    /**
     * Display a listing of the assignment.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->userRole() != 'student'){
            // Get all the assignments(display assignments per page)
            $assignments = Assignment::all();
        }else{

            //create an array to hold the result of the courses that the student registered in
            $courses = Course_Student::where('student_id', $this->userId())->pluck('course_id');

            //select all the assignments in the course that the student registered in
            $assignments = Assignment::whereIn('course_id', $courses)->get();
        }

        //Get all the courses that the lecturer is registered into
        $courses = DB::table('course_lecturer')
                        ->join('courses', 'courses.id', '=', 'course_lecturer.course_id')
                        ->where('course_lecturer.lecturer_id', '=', $this->lecturerId())
                        ->select('courses.id as id' , 'courses.title as title')
                        ->get();

        //return the assignment view and pass in the assignments and courses retrieved from the database
        return view(
                      'assignment.index',
                      ['assignments'=>$assignments, 'courses'=>$courses]
                    );


    }

    /**
     * Store a newly created assignment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //return $request->all();

        if($this->loggedIn()){
            $assignment_exists = Assignment::where('course_id', $request->input('course'))->where('user_id', $this->userId())->where('title',$request->input('title'))->get();
             
             //create a new assignment object instance
                if(count($assignment_exists) > 0){

                   
                    //set a session notification
                    $this->setSession('error', 'Assignment Exists');
                    //redirect the request back to where it came from and also include the inputs the user sent
                    return redirect()->route('assignments.index');
                }else{
                    $assignment = new Assignment;

                    //Instantiate all the properties of the assignment model
                    //i.e all the fields in the database
                    $assignment->title        = $request->input('title');
                    $assignment->description  = $request->input('desc');
                    $assignment->percentage  = $request->input('percentage');
                    $assignment->course_id    = $request->input('course');
                    $assignment->user_id      = $this->userId();

                    //Save the new assignment record in the assignments table
                    $assignment->save();


            //check if the assignment was successfully created?
            if($assignment->id){

                //The variable to hold the attachment that is associated with
                //the assignment
                $attachment = null;

                //check if any file was attached to the request
                if ($request->hasFile('attachments')) {

                    //loop through all the attachments (there could be multiple files)
                    foreach($request->file('attachments') as $attach){

                        //Create a new Aupload instance i.e this is the instance of the model 
                        $aupload = new Aupload;

                        //creating some utility variables
                        $filename='';
                        $UserUploadedImageURL='';

                        //configure the file name based on the current time stamp.
                        $filename=time().'.'.$attach->getClientOriginalExtension();

                        //move the new file to the right folder
                        $attachment=$attach->move(public_path('uploads\assignments'), $filename);

                        //configure the insert parameters
                        $aupload->assignment_id =  $assignment->id;
                        $aupload->upload_path =  $attachment;
                        $aupload->mime_type  =  $attach->getClientMimeType();

                        //save the new upload records
                        $aupload->save();
                    }
                    //set a session notification
                    $this->setSession('success', 'Assignment was successfully created!');
                    //redirect to the route that shows all the assignments
                    return redirect()->route('assignments.index');
                }
                 //set a session notification
                 $this->setSession('success', 'Assignment was successfully created!');
                 //redirect to the route that shows all the assignments
                 return redirect()->route('assignments.index');
            }else{
                
                 //set a session notification
                 $this->setSession('error', 'Please make sure that all the fields were filled correctly');

                 //redirect the request back to where it came from and also include the inputs the user sent
                 return redirect()->back()->withInput();
            }
                }


        }else{
           //set a session notification
            $this->setSession('warning', 'You must be logged in to perform this action');
            return redirect()->route('home');
        }


    }



    /**
     * Remove the specified assignment from storage.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
      

            //find all the assignments and delete it
            if($assignment->delete()){
                //set a session notification
                $this->setSession('success', 'Assignment was successfully deleted!');
                //redirect to the route that shows all the assignments
                return redirect()->route('assignments.index');
            }else{
                //set a session notification
                $this->setSession('error', 'Assignment was not successfully deleted!');
                //redirect to the route that shows all the assignments
                return redirect()->route('assignments.index');
            }
      
    }

    public function submitShow($assignment){
            //create an array to hold the result of the courses that the student registered in
            $courses = Course_Student::where('student_id', $this->userId())->pluck('course_id');

            //select all the assignments in the course that the student registered in
            $assignments = Assignment::whereIn('course_id', $courses)->paginate(5);

            //select all the assignments in the course that the student registered in
            $single_assignment = Assignment::where('id', $assignment)->get()->first();
            
            //Get all the courses that the lecturer is registered into
            $courses =  DB::table('course_lecturer')
                        ->join('courses', 'courses.id', '=', 'course_lecturer.course_id')
                        ->where('course_lecturer.lecturer_id', '=', $this->lecturerId())
                        ->select('courses.id as id' , 'courses.title as title')
                        ->get();

            return view(
                        'assignment.submit',
                        ['assignments'=>$assignments, 'courses'=>$courses, 'assignment' => $single_assignment]
                       );
    }
}
