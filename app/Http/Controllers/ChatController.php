<?php

namespace App\Http\Controllers;

use App\User;
use App\Chat;
use App\Http\Controllers\UpdateChatController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ChatInterface;

/**
* ChatController is a class for handling all the tasks related to chats
* The actions here are all triggered through ajax request
*
* @author   Stanley
* @author   Ben
* @date     3/3/2018
*/


//CREATE AN INTERFACE THAT WILL INHERIT ALL THE METHODS WE ARE USING HERE...  ChatInterface.php
class ChatController extends Controller implements ChatInterface
{

   /**
     * Store the chat message in the chats table
     *
     * @return \Illuminate\Http\Request
     */ 
    public function store(Request $request){

        //check if the request is a post request
        if ($request->isMethod('post')){ 

            //create a new chat object
                $chat = new Chat;

                //set the parameters for the model
                $chat->chat_from = $this->userId();
                $chat->chat_to   = $request->chat_to;
                $chat->message   = $request->message;

                //save the chat message
                $chat_save = $chat->save();

                //if the chat was saved?
                if($chat_save){
                        return response()->json(['response' => 'Saved']); 
                }else{
                    return response()->json(['response' => 'Chat could not be sent']); 
                }
        }else{
            //if the method was wrong?
            return response()->json(['response' => 'Wrong method']); 
        }

    }


    public function getChats(){

         //get the last chat that the users
        $last_received = DB::table('chats')
            ->where('chats.chat_to', '=', Auth::id())
            ->orderBy('chats.created_at', 'desc')
            ->get();

        $response = null; //holds the HTML response
        $last_id  = null; //hold the last chat ID retrieved...this would determine if the chat should play a sound
        $last_received_count = 0;
        $count    = 0;

        //Check if the current logged user has a new message
        foreach($last_received as $record ){
            if($last_received_count < 1){
                $last_received_id = $record->id;
                $last_received_count++;
            }
        }


        
        if($last_received_id != null)
        {
            //check if there  is a new msg..... 
            $update = new UpdateChatController(1);
            
            //Instantiate the object of UpdateChatController
            return $update->updateStates(1);
        }


    }
    

      /**
     * 
     * Get all users to populate the select the dropdown
     *
     */ 
    function getAllChatTo(){
        //get all users
        $users = User::all();

        $response = null;

        //if users were found?
        if(count($users) > 0 ){

            //build the response
            $response.='<select class="form-control emailhide" id="chat_to"> ';
            $response.='<option  value="">Select Recipient</option>';

            //loop through the select option fields
            foreach($users as $user){
                $response.='<option value="'.$user->id.'">'.$user->name.' - '.$user->email.'</option>';
            }

            $response.='</select>';
        }else{
            $response.= '<p class="emailhide">No recipient was found/p>';
        }

        //return the response to the calling method
        return response()->json(['response' => $response]); 
    }

    /**
     * 
     * Get filtered users to populate the select the dropdown
     *  @return \Illuminate\Http\Request
     */
    function getChatRecipient(Request $request){

        //get a user where the role is right?
        $users = User::where('role', $request->role)->get();
        $response = null;

        //check is data was returned
        if(count($users) > 0 ){

            //build the response
            $response.='<select class="form-control emailhide" id="chat_to"> ';
            $response.='<option  value="">Select Recipient</option>';

            //build the select options
            foreach($users as $user){
                $response.='<option value="'.$user->id.'">'.$user->name.' - '.$user->email.'</option>';
            }
            $response.='</select>';
        }else{
            $response.= '<p class="emailhide">No recipient was found</p>';
        }
           
        

        return response()->json(['response' => $response]); 

    }
}
