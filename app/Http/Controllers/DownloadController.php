<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Aupload;
use App\Email;

class DownloadController extends Controller
{
    //

        public function downloadFile($file){


            
            $fileRecord = Aupload::find($file);
            $file   = $fileRecord->upload_path;
            $mime  = $fileRecord->mime_type;


            header('Content-Description: File Transfer');
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            flush(); // Flush system output buffer
            readfile($file);
            exit;



            
            // $headers =[
            //     'Content-Type: '.$mime,
            //     'Content-Description: File Transfer',
            //     'Content-Disposition: attachment; filename="'.basename($file).'"',
            //     'Expires: 0',
            //     'Cache-Control: must-revalidate',
            //     'Pragma: public',
            //     'Content-Length: ' . filesize($file)
            // ];
  
            // return response()->download($file, 'New file', $headers);
            //return Response::download($file, 'output.csv', ['Content-Type:'.$mime]);
           // return response()->download($file, 'filename.pdf', );
            //return Response::download($file, 'filename.pdf', $headers);
        }



        public function downloadEmail($file){
            
            $fileRecord = Email::find($file);
            $file   = $fileRecord->attachment;
            $mime  = $fileRecord->mime_type;


            header('Content-Description: File Transfer');
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            flush(); // Flush system output buffer
            readfile($file);
            exit;


 
        }


}
