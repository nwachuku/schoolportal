<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Display a listing of the program.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all the programs
        $programs = Program::orderby('title')->get();
       
        return view(
                        'programs.index',
                        ['programs' => $programs]
                   );
    }


    /**
     * Store a newly created program in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create a new instance of the program
        $program = new Program;

        //Configure the query parameters
        $program->title       = $request->input('title');
        $program->description =  $request->input('desc');

        //Save the record
        $status = $program->save();

        if($status){
             //set a session notification
             $this->setSession('success', 'Program was successfully created!');
             return  redirect()->route('programs.index');

        }else{
            //set a session notification
            $this->setSession('error', 'Program was not successfully created!');
            return  redirect()->route('programs.index');
        }
    }


    /**
     * Show the form for editing the specified program.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Find the specific program
        $program  = Program::find($id);

        //Get all programs
        $programs = Program::all();

        return view(
                        'programs.edit', 
                        ['program' => $program, 'programs' => $programs ]
                   );

    }

    /**
     * Update the specified program in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            //Find the specific program
            $program = Program::find($id);

            //configure the query parameters
            $program->title = $request->input('title');
            $program->description =  $request->input('desc');

            //update program
            $update_status = $program->update();

            if($update_status){
                 //set a session notification
                 $this->setSession('success', 'Program was successfully updated!');
                 return redirect()->back();
            }else{
                //set a session notification
                $this->setSession('error', 'Program was not successfully updated!');
                return redirect()->back();
            }

    }

    /**
     * Remove the specified program from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $program = Program::find($id);

        if($program->delete()){
            $this->setSession('success', 'Program was successfully deleted!');
            return redirect()->route('programs.index');
        }else{
            $this->setSession('error', 'Program was not successfully deleted!');
            redirect()->route('programs.index');
        }

        
    }
}
