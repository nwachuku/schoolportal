<?php

namespace App\Http\Controllers;

use App\User;
use App\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


interface ChatInterface {

    public function store(Request $request);

    public function getChats();

    public function getAllChatTo();
    
    public function getChatRecipient(Request $request);

} 