<?php

namespace App\Http\Controllers;

use App\User;
use App\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


interface UpdateChatInterface {

    public function updateView();

} 