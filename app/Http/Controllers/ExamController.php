<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Program;
use App\Course_Student;
use App\Course;
use App\Lecturer;
use App\Assignment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($this->userRole() == 'lecturer'){
                //
                $programs = Program::all();
                $assignments = Assignment::where('user_id', $this->userId())->get();
                $exams   = Exam::all();
                // $exams =  DB::table('exams')
                //         ->join('users', 'users.id', '=', 'exams.student_id')
                //         ->join('courses', 'courses.id', '=', 'exams.course_id')
                //         ->where('exams.lecturer_id', '=', $this->userId())
                //         ->orderBy('exams.created_at', 'desc')
                //         ->get();
             $lecturers  = Lecturer::where('user_id', Auth::id())->get();

             return view('exams.index',
                                [
                                    'lecturers'  => $lecturers,
                                    'exams'      => $exams,
                                    'assignments' => $assignments
                                ]
                            );
        }else{

            $exams =  DB::table('exams')
                    ->join('users', 'users.id', '=', 'exams.student_id')
                    ->join('courses', 'courses.id', '=', 'exams.course_id')
                    ->where('exams.student_id', '=', $this->userId())
                    ->orderBy('exams.created_at', 'desc')
                    ->get();

                    return view('exams.index',
                    [
                        'exams'      => $exams
                    ]
                );
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate - Check if student was added
       $validation_result =  $request->validate([
            'student' => 'bail|required',
            'type' => 'required',
        ]);

       // return $request->all();

        $exam = new Exam;

        $exam->assignment_id =  $request->type;
        $per_score = ((int)$request->score/(int)$request->percentage)*100;
        $exam->score    = $request->score;
        $exam->percentage = $request->percentage;
        $exam->percentage_score = floor($per_score * 100) / 100 ;
        $exam->course_id = $request->course;
        $exam->lecturer_id = $this->userId();
        $exam->student_id = $request->student;

        if($exam->save()){
            $this->setSession('success', 'Assessment was successfully created!');
            return redirect()->route('exams.index');
        }else{
            $this->setSession('error', 'Assessment was not successfully created!');
             return redirect()->back()->withInput();
        }
    }

    /**
     * Update the exam total score
     *
     * @param   $exam
     * 
     */
    public function updateTotal($exam)
    {
        //Get each of those scores
        $assignment1 = DB::table('exams')->where('id', $exam)->value('assignment1');
        $assignment2 = DB::table('exams')->where('id', $exam)->value('assignment2');
        $assignment3 = DB::table('exams')->where('id', $exam)->value('assignment3');
        $quiz1   = DB::table('exams')->where('id', $exam)->value('quiz1');
        $quiz2   = DB::table('exams')->where('id', $exam)->value('quiz2');
        $quiz3   = DB::table('exams')->where('id', $exam)->value('quiz3');
        $midterm = DB::table('exams')->where('id', $exam)->value('midterm');
        $final   = DB::table('exams')->where('id', $exam)->value('final');

        //add them to the total and update the exam total
        $total = $assignment1+$assignment2+$assignment3+$quiz1+$quiz2+$quiz3+$midterm;

        $examObj = Exam::find($exam);

        $examObj->total = $total;

        if($examObj->update()){
            return true;
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        //



        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam $exam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exam  $exam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        //
    }

    public function get_type($type){
        
        $grading_type = null;

        if($type == 'quiz1'){
            $grading_type = $type;
        }elseif($type == 'quiz2'){
            $grading_type = $type;
        }elseif($type == 'quiz3'){
            $grading_type = $type;
        }elseif($type == 'midterm'){
            $grading_type = $type;
        }elseif($type == 'final'){
            $grading_type = $type;
        }

        return $grading_type;

    }



    public function fetchPercentage($id){
        $percentage = Assignment::where('id', $id)->where('user_id', $this->userId())->value('percentage');
        return $percentage;
    }
}
