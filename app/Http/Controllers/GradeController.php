<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$exams   = Exam::all();
        $exams   = DB::select( DB::raw("SELECT DISTINCT `student_id`, `course_id`  FROM `exams` WHERE lecturer_id = :lecturer_id"), array(
            'lecturer_id' => $this->userId(),
          ));


        $student_exam_record = DB::select( DB::raw("SELECT DISTINCT `student_id`  FROM `exams` WHERE lecturer_id = :lecturer_id"), array(
            'lecturer_id' => $this->userId(),
          ));

          $student_course_record = DB::select( DB::raw("SELECT DISTINCT  `course_id`  FROM `exams` WHERE lecturer_id = :lecturer_id"), array(
            'lecturer_id' => $this->userId(),
          ));

         
        if($this->userRole() == 'student'){
            $grades  = Grade::where('user_id', $this->userId())->get();
        }elseif($this->userRole() == 'lecturer'){
            $grades  = Grade::all();
        }

        return view('grades.index', 
            [
                'exams' => $exams,
                'grades' => $grades,
                'student_exam_record' => $student_exam_record,
                'student_course_record' => $student_course_record
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //Get the total count of  students in the exam table
       $student_count = Exam::where('student_id', $request->student)->count();
       $total_percent = Exam::where('student_id', $request->student)
                        ->pluck('percentage_score')
                        ->sum();

       $grade_percent = $total_percent/$student_count;

       $students  = $request->student;
        if(sizeof($students) > 0){
            foreach ($students as $student) {
               
                //Find if a grade already exists;

                $grade = Grade::where('user_id', $student)->where('course_id', $request->course)->first();
                
                if($grade){

                        $grade->user_id   = $student;
                        $grade->course_id = $request->course;
                        $grade->grade     = floor($grade_percent * 100) / 100;

                        if($grade->update()){
                            $this->setSession('success', 'Grade was successfully updated!');
                        }else{
                            $this->setSession('error', 'Grade could not be updated!');
                        }

                    }else{

                        $grade = new Grade;

                        $grade->user_id   = $student;
                        $grade->course_id = $request->course;
                        $grade->grade     = floor($grade_percent * 100) / 100;

                        if($grade->save()){
                            $this->setSession('success', 'Grade was successfully created!');
                        }else{
                            $this->setSession('error', 'Grade could not be created!');
                        }
                    }

                    return redirect()->route('grades.index');
            }

        }else{

            $this->setSession('error', 'Could not find a student to update the grade!');
            return redirect()->route('grades.index');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Grade $grade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grade $grade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        //
    }
}
