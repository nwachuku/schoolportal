<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\LecturerController;
use App\Http\Controllers\StudentController;

class UserFactoryController extends Controller
{


    public function __construct($request, $role)
    {
        return $this->makeUser($request, $role);
    }

    public function makeUser($request, $role)
    {
        if($role == 'student'){
            $student = new StudentController;
            return $student->store($request);
        }elseif($role == 'lecturer'){
            $lecturer = new LecturerController;
            return $lecturer->store($request);
        }
    }
}
