<?php

namespace App\Http\Controllers\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\SubmitAssignment;
use App\Assignment;
use App\Course;
use App\Course_Student;
use App\Event;
use App\Student;
use App\Lecturer;
use App\User;
class HelperController extends Controller
{
    //
    public function checkSubmitted($assignment){
        //check if that specific assignment has been submitted by the student
        $result = SubmitAssignment::where('assignment_id', $assignment)->where('student_id', $this->userId())->get();
        
        return (count($result) > 0) ? true : false;
    }

    public function getStudentProgram(){
       
        if(Auth::user()->role == 'student'){
            return Student::where('user_id', Auth::id())->value('program_id');
        }

    }

    public function getEventCount(){
        //Check the type of user that logged in..

        //Student
        if(Auth::user()->role == 'student'){
            return Event::where('program_id', $this->getStudentProgram())->where('date', '>', now())->count();
        }elseif(Auth::user()->role == 'lecturer' || Auth::user()->role == 'admin'){
            return Event::where('date', '>', now())->count();
        }

    }

    public function getuserFullName($id){

            $user = User::find($id);

            return $user->name;

    }

    public function getCourseName($id){

        $course = Course::find($id);

        return $course->title;

}

    public function getuserEmail($id){
        $user = User::find($id);
        return $user->email;
    }
 
    public function prettyDate($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y");
        return $newDate;
    }

    public function prettyTime($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"H:i:A");
        return $newDate;
    }

 
}
