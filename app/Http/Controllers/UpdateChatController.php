<?php

namespace App\Http\Controllers;

use App\User;
use App\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UpdateChatInterface;
 /**
* ChatController is a class for handling all the tasks related to chats
* The actions here are all triggered through ajax request
*
* @author   Stanley
* @author   Ben
* @date     3/3/2018
*/


//CREATE AN INTERFACE THAT WILL INHERIT ALL THE METHODS WE ARE USING HERE...  ChatInterface.php
class UpdateChatController extends Controller implements  UpdateChatInterface
{

  
    public function updateStates($status){
        return $this->updateView();
        
     }

   
    /**
     * get chats at certain intervals
     *
     * @return \Illuminate\Http\Request
     */ 
    public function updateView(){



        //Get all chats
        $messages = DB::table('chats')
                    ->join('users', 'users.id', '=', 'chats.chat_from')
                    ->where('chats.chat_from', '=', Auth::id())
                    ->orWhere('chats.chat_to', '=', Auth::id())
                    ->orderBy('chats.created_at', 'desc')
                    ->select('chats.id as id' , 'users.name as name','chats.message as message' ,'chats.chat_from as chat_from', 'chats.chat_to as chat_to')
                    ->get();

        //get the last chat that the users
        $last_received = DB::table('chats')
                        ->where('chats.chat_to', '=', Auth::id())
                        ->orderBy('chats.created_at', 'desc')
                        ->get();

            $response = null; //holds the HTML response
            $last_id  = null; //hold the last chat ID retrieved...this would determine if the chat should play a sound
            $last_received_count = 0;
            $count    = 0;

            //Check if the current logged user has a new message
            foreach($last_received as $record ){
                if($last_received_count < 1){
                    $last_received_id = $record->id;
                    $last_received_count++;
                }
            }

            session(['last_id' => $last_received_id]);

            //build the response
            $response.="<div id='remove' class='panel-body'>";
            $response.="<ul style='overflow-y: scroll; max-height: 180px;' id='chatMessages' class='media-list messages nicescroll' style='overflow: hidden;' tabindex='5001'>";
            
            //loop through the messages
            foreach($messages as $message ){

                //message sent from the currently logged in user
                if($message->chat_from == Auth::id()){
                    $response.= "<li class='media'>";
                        $response.= "<a href='#' class='pull-left user-status online'>";
                        $response.=     "<img alt='user' class='media-object' src='".asset('assets/images/avtar/image1.png')."'>";
                        $response.= "</a>";
                        $response.= "<div class='media-body'>";
                            $response.= "<h5 class='media-heading text-left'><strong>".$message->name."</strong></h5>";
                            $response.= "<p class='text-muted no-margn text-left'>".$message->message."</p>";
                        $response.= "</div>";
                    $response.= "</li>";

                    if($count < 1){
                        $last_id = $message->id;
                        $count++;
                    }

                }else{ //message received by the currently logged in user

                    
                    $response.= "<li class='media'>";
                    $response.= "<a href='#' class='pull-right user-status offline'>";
                    $response.= "<img alt='user' class='media-object' src='".asset('assets/images/avtar/image2.png')."'>";
                    $response.= "</a>";
                    $response.= "<div class='media-body'>";
                    $response.= "<h5 class='media-heading'><strong>".$message->name."</strong></h5>";
                    $response.= "<p class='text-muted no-margn'>".$message->message."</p>";
                    $response.= "</div>";
                    $response.= "</li>";

                    if($count < 1){
                        $last_id = $message->id;
                        $count++;
                    }
                }

            }
            $response.="</ul>";
            $response.="</div>";

            return response()->json(['response' => $response, 'lastID' => $last_id, 'lastRecievedID' => $last_received_id]);
    }


}
