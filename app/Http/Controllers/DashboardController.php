<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Event;
use App\Course;
use App\Exam;
use App\Program;
use App\Student;
use App\Lecturer;
use App\Course_Student;
use App\SubmitAssignment;
use App\Grade;

class DashboardController extends Controller
{

    public function __construct()
    {
        //ONLY LOGGED IN USERS CAN ACCESS THIS CONTROLLER
        $this->authUser();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->userRole() === 'student'){
                  //
                 $program_id = Student::where('user_id', $this->userId())
                ->select('program_id')->first();
                $current_program = Program::find($program_id);
                $number_of_events = Event::where('program_id', 1)->where('date', '>', now())->orderby('date')->count();
                $events   = DB::table('events')->where('program_id',1)->where('date', '>', now())->orderby('date')->paginate(5);
                $number_of_registered_courses = Course_Student::where('student_id', $this->userId())
                                    ->count();


                $grade = Grade::where(['user_id' => $this->userId()])->value('grade');                   

                return view('welcome',
                    [
                        'current_program' => $current_program,
                        'number_of_registered_courses' => $number_of_registered_courses,
                        'grade' => $grade,
                        'number_of_events' => $number_of_events,
                        'events' => $events,
                    ]
                );

        }elseif($this->userRole() === 'lecturer'){

                $courses             = DB::table('assignments')->where('user_id', $this->userId())->pluck('id');
                $assignments_count   = SubmitAssignment::whereIn('assignment_id', $courses)->count();
                $number_of_events    = Event::where('date', '>', now())->orderby('date')->count();
                $events              = DB::table('events')->where('date', '>', now())->orderby('date')->paginate(5);
                $degree              = Lecturer::where('user_id', $this->userId())->value('degree');

                return view('welcome',
                [
                    'events' => $events,
                    'number_of_events'=> $number_of_events,
                    'assignments_count' => $assignments_count,
                    'degree' => $degree,
                ]
            );   
        }elseif($this->userRole() === 'admin'){
            $events              = DB::table('events')->where('date', '>', now())->orderby('date')->paginate(5);
            $number_of_events    = Event::where('date', '>', now())->orderby('date')->count();
            $total_students      = Student::count();
            $total_lecturers      = Lecturer::count();
            return view('welcome',
            [
                'events' => $events,
                 'total_lecturers'=> $total_lecturers,
                 'total_students' => $total_students,
                 'number_of_events' => $number_of_events,
            ]);
        }
      
    }

}
