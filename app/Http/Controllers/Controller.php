<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Session;
use App\Lecturer;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function setSession($type, $message){
        Session::flash($type, $message);
    }

    public function loggedIn(){

        return Auth::check();
    }
    public function userId(){
        return Auth::user()->id;
    }

    public function userRole(){
        return Auth::user()->role;
    }

    public function lecturerId(){
        $lecturer = Lecturer::where('user_id', $this->userId())->get()->take(1);

        foreach($lecturer as $record){
            return $record->id;
        }
    }

    public function authUser(){
        $this->middleware('auth');
    }
}
