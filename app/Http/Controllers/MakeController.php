<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Student;
use App\Lecturer;
use App\Http\Controllers\UserFactoryController;

class MakeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validity_check = User::where('email',$request->email)->first();
        if($request->role == 'student'){

            $phone_validity_check = Student::where('phone',$request->phone)->first();

        }else{
             $phone_validity_check = Lecturer::where('phone',$request->phone)->first();
        }

        
        if($validity_check != null){
            //return 'exits';
            $this->setSession('error', 'User with the same email already exists!');

        }
        elseif($phone_validity_check != null){
            //return 'exits';
            $this->setSession('error', 'User with the same phone number already exists!');
        }else{
            //return 'no exists';
            $factory = new UserFactoryController($request, $request->role);
        }


        return redirect()->back()->withInput();
    }

  
}
