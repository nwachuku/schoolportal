<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'date', 'time', 'description','color', 'creator'
    ];


    public function program(){
        return $this->belongsTo('App\Program');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }


}
