<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    //


    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }
}
