<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mupload extends Model
{
    //

    public function material(){
        return $this->belongsTo('App\Material');
    }
    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }
}
