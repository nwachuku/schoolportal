<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    //


    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function student(){
        return $this->belongsTo('App\Student');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }



}
