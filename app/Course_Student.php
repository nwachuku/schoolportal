<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_Student extends Model
{
    //
    protected $table = 'courses_students';

    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function student(){
        return $this->belongsTo('App\Student');
    }


}
