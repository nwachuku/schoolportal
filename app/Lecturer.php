<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    //

    public function getGenderAttribute($value)
    {
        return ucfirst($value);
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'course_lecturer')->using('App\CourseLecturer');
    }

    public function lecturercourse(){
        return $this->hasMany('App\LecturerCourse');
    }

    public function exams(){
        return $this->hasMany('App\Exam');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }
}
