<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    //Relationships
    public function email(){
        return $this->hasMany('App\Email');
    }
    public function students(){
        return $this->hasOne('App\Student');
    }

    public function lecturers(){
        return $this->hasOne('App\Lecturer');
    }

    public function exam(){
        return $this->hasMany('App\Exam');
    }

    public function grade(){
        return $this->hasMany('App\Grade');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }


}
