<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CourseLecturer extends Pivot
{
    //
    protected $table = 'course_lecturer';

    public function courses(){
        return $this->hasMany('App\Course');
    }
}
