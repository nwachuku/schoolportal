<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    public function lecturers()
    {
        return $this->belongsToMany('App\Lecturer', 'course_lecturer')->using('App\CourseLecturer');
    }

    public function materials(){
        return $this->hasMany('App\Material');
    }

    public function assignments(){
        return $this->hasMany('App\Assignment');
    }

    public function course_student(){
        return $this->hasMany('App\Course_Student');
    }

    public function lecturercourse(){
        return $this->belongsToMany('App\LecturerCourse');
    }

    public function exams(){
        return $this->hasMany('App\Exam');
    }

    public function grade(){
        return $this->hasMany('App\Grade');
    }


}
