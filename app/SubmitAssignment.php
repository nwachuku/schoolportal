<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmitAssignment extends Model
{
    //
    protected $table = 'submitassigments';

    public function assignment(){
        return $this->belongsTo('App\Assignment');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }
}
