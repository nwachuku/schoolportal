<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    //
    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function exam(){
        return $this->hasMany('App\Exam');
    }

    public function auploads(){
        return $this->hasMany('App\Aupload');
    }

    public function submission(){
        return $this->hasMany('App\SubmitAssignment');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }

}
