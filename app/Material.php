<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    public function course(){
        return $this->belongsTo('App\Course');
    }

    public function muploads(){
        return $this->hasMany('App\Mupload');
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        $newDate = date_format($date,"M jS Y @ H:i:s");
        return $newDate;
    }
}
